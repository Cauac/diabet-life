package life.diabet.diabetlife;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.DatePicker;
import android.widget.TextView;

import com.cocosw.bottomsheet.BottomSheet;

import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Calendar;

import life.diabet.diabetlife.dialog.record.EditRecordActivity;
import life.diabet.diabetlife.fragment.ComponentInitializationListener;
import life.diabet.diabetlife.fragment.RecordChartFragment;
import life.diabet.diabetlife.fragment.RecordListFragment;
import life.diabet.diabetlife.util.FormatUtil;
import life.diabet.diabetlife.util.UiUtil;

public class HomeActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    public static final int ADD_RECORD_CODE = 1;
    public static final int EDIT_RECORD_CODE = 2;

    private Calendar calendar = Calendar.getInstance();
    private TextView dateLabel;
    private RecordListFragment recordList;
    private RecordChartFragment recordChart;
    private RecordDbHelper dbHelper;
    private int itemPosition;

    private View.OnClickListener onDateClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new DatePickerDialog(HomeActivity.this, HomeActivity.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
        }
    };

    private OnItemClickListener onItemClickListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            itemPosition = position;
            editRecord(recordList.getRecord(position));
        }
    };

    private ComponentInitializationListener fragmentInitializationListener = new ComponentInitializationListener() {
        @Override
        public void componentsReady() {
            updateView();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        dateLabel = (TextView) findViewById(R.id.dateLabel);
        dateLabel.setOnClickListener(onDateClick);

        dbHelper = RecordDbHelper.getInstance(this);

        if (findViewById(R.id.fragment_container) != null) {
            if (savedInstanceState != null) {
                return;
            }
            recordList = new RecordListFragment();
            recordList.setComponentInitializationListener(fragmentInitializationListener);
            recordList.setItemClickListener(onItemClickListener);
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, recordList).commit();
        }
    }

    public void updateView() {
        dateLabel.setText(FormatUtil.dateToString(calendar.getTime()));
        if (recordList != null) {
            recordList.show(dbHelper.list(DateFormatUtils.ISO_DATE_FORMAT.format(calendar)));
        }
        if(recordChart!=null){
            recordChart.show(dbHelper.list(DateFormatUtils.ISO_DATE_FORMAT.format(calendar)));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void minusDay(View view) {
        calendar.add(Calendar.DATE, -1);
        updateView();
    }

    public void plusDay(View view) {
        calendar.add(Calendar.DATE, 1);
        updateView();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        updateView();
    }

    public void onViewChange(View view){
        recordChart=new RecordChartFragment();
        recordChart.setItemClickListener(onItemClickListener);
        recordChart.setComponentInitializationListener(fragmentInitializationListener);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, recordChart).commit();
    }

    public void onAddButtonClick(View view) {
        final String title = getResources().getString(R.string.add_sheet_title);
        new BottomSheet.Builder(this).title(title).grid().sheet(R.menu.bottom_sheet).listener(onAddRecordClick).show();

    }

    private DialogInterface.OnClickListener onAddRecordClick = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            Intent intent = new Intent(HomeActivity.this, UiUtil.SHEET_ITEMS.get(which));
            startActivityForResult(intent, ADD_RECORD_CODE);
        }
    };

    public void editRecord(Record record) {
        Class dialogClass = UiUtil.RECORD_DIALOGS.get(record.getType());
        Intent intent = new Intent(this, dialogClass);
        intent.putExtra(EditRecordActivity.RECORD_EXTRA, record);
        startActivityForResult(intent, EDIT_RECORD_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (RESULT_OK == resultCode) {
            switch (requestCode) {
                case ADD_RECORD_CODE:
                    Record record = ((Record) data.getSerializableExtra(EditRecordActivity.RECORD_EXTRA));
                    record.setDate(DateFormatUtils.ISO_DATE_FORMAT.format(calendar));
                    dbHelper.save(record);
                    if (recordList != null) {
                        recordList.add(record);
                    }
                    break;
                case EDIT_RECORD_CODE:
                    Record oldRecord = recordList.getRecord(itemPosition);
                    recordList.remove(oldRecord);
                    if (data == null) {
                        dbHelper.delete(oldRecord);
                    } else {
                        Record updatedRecord = (Record) data.getSerializableExtra(EditRecordActivity.RECORD_EXTRA);
                        updatedRecord.merge(oldRecord);
                        recordList.insert(itemPosition, updatedRecord);
                        dbHelper.update(updatedRecord);
                    }
            }
        }
    }
}
