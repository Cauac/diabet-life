package life.diabet.diabetlife.dialog.value;

import life.diabet.diabetlife.R;

public class FoodValueDialog extends ValueDialog {

    public final static String DIALOG_NAME = "life.diabet.diabetlife.dialog.value.FoodValueDialog";

    @Override
    public String getDialogName() {
        return DIALOG_NAME;
    }

    @Override
    protected String getTitle() {
        return getResources().getString(R.string.food_title);
    }

    @Override
    protected String getValueLabelText() {
        return getResources().getString(R.string.food_unit);
    }

    @Override
    protected int getMinValue() {
        return getResources().getInteger(R.integer.food_dialog_min_value);
    }

    @Override
    protected int getMaxValue() {
        return getResources().getInteger(R.integer.food_dialog_max_value);
    }

    @Override
    protected String getDefaultValue() {
        return getResources().getString(R.string.food_dialog_default_value);
    }
}
