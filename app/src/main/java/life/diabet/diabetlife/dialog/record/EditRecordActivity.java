package life.diabet.diabetlife.dialog.record;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;

import life.diabet.diabetlife.R;
import life.diabet.diabetlife.Record;
import life.diabet.diabetlife.dialog.value.ValueDialog;
import life.diabet.diabetlife.dialog.value.ValueDialog.ValueDialogListener;
import life.diabet.diabetlife.util.FormatUtil;

public abstract class EditRecordActivity extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener, ValueDialogListener {

    public static final String RECORD_EXTRA = "life.diabet.diabetlife.EditRecordActivity.RECORD_EXTRA";

    protected boolean isEditRecordMode;
    protected String valueUnit;

    protected EditText timeField;
    protected EditText valueField;
    protected EditText commentField;

    private View.OnClickListener onTimeClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final Calendar c = FormatUtil.stringToTime(timeField.getText().toString());
            new TimePickerDialog(EditRecordActivity.this, EditRecordActivity.this, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true).show();
        }
    };

    private View.OnClickListener onValueClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ValueDialog valueDialog = getValueDialog();
            valueDialog.setValue(getValue());
            valueDialog.show(getFragmentManager(), valueDialog.getDialogName());
        }
    };

    protected abstract int getViewResource();

    protected abstract String getValueUnit();

    protected abstract ValueDialog getValueDialog();

    protected abstract String getDefaultValue();

    protected abstract String getRecordType();

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        timeField.setText(FormatUtil.intToStringTime(hourOfDay, minute));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isEditRecordMode) {
            getMenuInflater().inflate(R.menu.menu_edit_dialog, menu);
            return true;
        } else {
            return super.onCreateOptionsMenu(menu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT_CANCELED);
                finish();
                return true;
            case R.id.action_delete:
                onDeleteClick();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onValueChosen(String value) {
        setValue(value);
    }

    protected void initFields() {
        timeField = (EditText) findViewById(R.id.timeField);
        timeField.setOnClickListener(onTimeClick);

        valueField = (EditText) findViewById(R.id.valueField);
        valueField.setOnClickListener(onValueClick);

        commentField = (EditText) findViewById(R.id.commentField);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getViewResource());

        valueUnit = getValueUnit();
        initFields();
        Object recordObject;
        if ((recordObject = getIntent().getSerializableExtra(RECORD_EXTRA)) != null) {
            isEditRecordMode = true;
            setRecord((Record) recordObject);
        } else {
            timeField.setText(FormatUtil.currentTime());
            setValue(getDefaultValue());
        }
    }

    public void onSaveClick(View view) {
        Intent answerIntent = new Intent();
        answerIntent.putExtra(RECORD_EXTRA, getRecord());
        setResult(RESULT_OK, answerIntent);
        finish();
    }

    public void onDeleteClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialog_delete_message);
        builder.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setResult(RESULT_OK);
                finish();
            }
        });
        builder.setNegativeButton(R.string.Cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    protected void setValue(String value) {
        valueField.setText(FormatUtil.recordValueToString(value, valueUnit));
    }

    protected String getValue() {
        return FormatUtil.extractValue(valueField.getText().toString());
    }

    protected void setRecord(Record record) {
        setValue(record.getValue());
        timeField.setText(record.getTime());
        commentField.setText(record.getComment());
    }

    protected Record getRecord() {
        Record record = new Record();
        record.setTime(timeField.getText().toString());
        record.setValue(getValue());
        record.setType(getRecordType());
        record.setComment(commentField.getText().toString());
        return record;
    }
}
