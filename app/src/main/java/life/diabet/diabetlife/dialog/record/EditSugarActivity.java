package life.diabet.diabetlife.dialog.record;

import android.view.View;
import android.widget.EditText;

import life.diabet.diabetlife.R;
import life.diabet.diabetlife.Record;
import life.diabet.diabetlife.dialog.ActivityDialog;
import life.diabet.diabetlife.dialog.StressDialog;
import life.diabet.diabetlife.dialog.value.SugarValueDialog;
import life.diabet.diabetlife.dialog.value.ValueDialog;

public class EditSugarActivity extends EditRecordActivity implements StressDialog.StressDialogListener, ActivityDialog.ActivityDialogListener {

    private EditText stressField;
    private EditText activityField;

    private View.OnClickListener onStressClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            StressDialog stressDialog = new StressDialog();
            stressDialog.show(getFragmentManager(), StressDialog.DIALOG_NAME);
        }
    };

    private View.OnClickListener onActivityClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ActivityDialog activityDialog = new ActivityDialog();
            activityDialog.show(getFragmentManager(), ActivityDialog.DIALOG_NAME);
        }
    };

    @Override
    protected int getViewResource() {
        return R.layout.activity_edit_sugar;
    }

    @Override
    protected String getValueUnit() {
        return getResources().getString(R.string.sugar_unit);
    }

    @Override
    protected ValueDialog getValueDialog() {
        return new SugarValueDialog();
    }

    @Override
    protected String getDefaultValue() {
        return getResources().getString(R.string.sugar_dialog_default_value);
    }

    @Override
    protected String getRecordType() {
        return Record.Types.SUGAR;
    }

    @Override
    protected void setRecord(Record record) {
        super.setRecord(record);
        stressField.setText(record.getStress());
        activityField.setText(record.getActivity());
    }

    @Override
    protected Record getRecord() {
        Record record = super.getRecord();
        record.setType(Record.Types.SUGAR);
        record.setActivity(activityField.getText().toString());
        record.setStress(stressField.getText().toString());
        return record;
    }

    @Override
    protected void initFields() {
        super.initFields();

        stressField = (EditText) findViewById(R.id.stressField);
        stressField.setOnClickListener(onStressClick);

        activityField = (EditText) findViewById(R.id.activityField);
        activityField.setOnClickListener(onActivityClick);
    }

    @Override
    public void onStressLevelChoose(String stress) {
        stressField.setText(stress);
    }

    @Override
    public void onActivityLevelChoose(String activity) {
        activityField.setText(activity);
    }
}
