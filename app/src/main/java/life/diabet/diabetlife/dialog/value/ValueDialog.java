package life.diabet.diabetlife.dialog.value;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;

import life.diabet.diabetlife.R;

public abstract class ValueDialog extends DialogFragment {

    public static final String VALUE = "life.diabet.diabetlife.dialog.value.ValueDialog.Value";

    public interface ValueDialogListener {
        void onValueChosen(String value);
    }

    protected ValueDialogListener mListener;
    protected TextView valueLabel;
    protected TextView fractionSeparator;
    protected NumberPicker decimalValuePicker;
    protected NumberPicker fractionValuePicker;
    protected String value;

    public abstract String getDialogName();

    protected abstract String getTitle();

    protected abstract String getValueLabelText();

    protected abstract int getMinValue();

    protected abstract int getMaxValue();

    protected abstract String getDefaultValue();

    protected DialogInterface.OnClickListener onOkClicked = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            mListener.onValueChosen(getValue());
            dialog.cancel();
        }
    };

    protected DialogInterface.OnClickListener onCancelClicked = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
        }
    };

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString(VALUE, getValue());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (ValueDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ValueDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.value_dialog, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setTitle(getTitle());
        builder.setPositiveButton(R.string.OK, onOkClicked);
        builder.setNegativeButton(R.string.Cancel, onCancelClicked);

        valueLabel = (TextView) view.findViewById(R.id.valueLabel);
        valueLabel.setText(getValueLabelText());

        fractionSeparator = (TextView) view.findViewById(R.id.fractionSeparator);

        decimalValuePicker = (NumberPicker) view.findViewById(R.id.decimalValuePicker);
        decimalValuePicker.setMinValue(getMinValue());
        decimalValuePicker.setMaxValue(getMaxValue());

        fractionValuePicker = (NumberPicker) view.findViewById(R.id.fractionValuePicker);
        fractionValuePicker.setMinValue(0);
        fractionValuePicker.setMaxValue(9);

        if (value == null) {
            value = savedInstanceState == null ? getDefaultValue() : savedInstanceState.getString(VALUE);
        }

        String[] valueParts = StringUtils.defaultString(value).split("\\.");
        int decimal = Integer.parseInt(valueParts[0]);
        int fractional = Integer.parseInt(valueParts[1]);
        decimalValuePicker.setValue(decimal);
        fractionValuePicker.setValue(fractional);

        return builder.create();
    }

    public void setValue(String valueStr) {
        if (StringUtils.isNoneBlank(valueStr)) {
            value = valueStr;
        }
    }

    public String getValue() {
        return String.format("%s.%s", decimalValuePicker.getValue(), fractionValuePicker.getValue());
    }
}
