package life.diabet.diabetlife.dialog.value;

import life.diabet.diabetlife.R;

public class SugarValueDialog extends ValueDialog {

    public static final String DIALOG_NAME = "life.diabet.diabetlife.dialog.value.SugarValueDialog";

    @Override
    public String getDialogName() {
        return DIALOG_NAME;
    }

    @Override
    protected String getTitle() {
        return getResources().getString(R.string.sugar_title);
    }

    @Override
    protected String getValueLabelText() {
        return getResources().getString(R.string.sugar_unit);
    }

    @Override
    protected int getMinValue() {
        return getResources().getInteger(R.integer.sugar_dialog_min_value);
    }

    @Override
    protected int getMaxValue() {
        return getResources().getInteger(R.integer.sugar_dialog_max_value);
    }

    @Override
    protected String getDefaultValue() {
        return getResources().getString(R.string.sugar_dialog_default_value);
    }
}
