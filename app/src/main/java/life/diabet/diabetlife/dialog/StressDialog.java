package life.diabet.diabetlife.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import life.diabet.diabetlife.R;

public class StressDialog extends DialogFragment {

    public static final String DIALOG_NAME = "life.diabet.diabetlife.dialog.StressDialog";

    protected StressDialogListener mListener;

    public interface StressDialogListener {
        void onStressLevelChoose(String stress);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (StressDialogListener) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final String[] stress_levels = getResources().getStringArray(R.array.stress_levels);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.stress_dialog_title);
        builder.setItems(stress_levels, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onStressLevelChoose(stress_levels[which]);
                dialog.cancel();
            }
        });
        return builder.create();
    }
}
