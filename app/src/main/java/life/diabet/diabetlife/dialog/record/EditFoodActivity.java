package life.diabet.diabetlife.dialog.record;

import life.diabet.diabetlife.R;
import life.diabet.diabetlife.Record;
import life.diabet.diabetlife.dialog.value.FoodValueDialog;
import life.diabet.diabetlife.dialog.value.ValueDialog;

public class EditFoodActivity extends EditRecordActivity {

    @Override
    protected int getViewResource() {
        return R.layout.activity_edit_food;
    }

    @Override
    protected String getValueUnit() {
        return getResources().getString(R.string.food_unit);
    }

    @Override
    protected ValueDialog getValueDialog() {
        return new FoodValueDialog();
    }

    @Override
    protected String getDefaultValue() {
        return getResources().getString(R.string.food_dialog_default_value);
    }

    @Override
    protected String getRecordType() {
        return Record.Types.FOOD;
    }
}
