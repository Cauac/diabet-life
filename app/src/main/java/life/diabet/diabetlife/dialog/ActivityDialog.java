package life.diabet.diabetlife.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import life.diabet.diabetlife.R;

public class ActivityDialog extends android.app.DialogFragment {

    public static final String DIALOG_NAME = "life.diabet.diabetlife.dialog.ActivityDialog";

    protected ActivityDialogListener mListener;

    public interface ActivityDialogListener {
        void onActivityLevelChoose(String activity);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mListener = (ActivityDialogListener) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final String[] activity_levels = getResources().getStringArray(R.array.activity_levels);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.activity_dialog_title);
        builder.setItems(activity_levels, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onActivityLevelChoose(activity_levels[which]);
                dialog.cancel();
            }
        });
        return builder.create();
    }
}
