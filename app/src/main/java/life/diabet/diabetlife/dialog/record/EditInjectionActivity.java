package life.diabet.diabetlife.dialog.record;

import life.diabet.diabetlife.R;
import life.diabet.diabetlife.Record;
import life.diabet.diabetlife.dialog.value.InjectionValueDialog;
import life.diabet.diabetlife.dialog.value.ValueDialog;

public class EditInjectionActivity extends EditRecordActivity {

    @Override
    protected int getViewResource() {
        return R.layout.activity_edit_injection;
    }

    @Override
    protected String getValueUnit() {
        return getResources().getString(R.string.injection_unit);
    }

    @Override
    protected ValueDialog getValueDialog() {
        return new InjectionValueDialog();
    }

    @Override
    protected String getDefaultValue() {
        return getResources().getString(R.string.injection_dialog_default_value);
    }

    @Override
    protected String getRecordType() {
        return Record.Types.INJECTION;
    }
}
