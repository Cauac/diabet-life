package life.diabet.diabetlife.dialog.value;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;

import life.diabet.diabetlife.R;

public class InjectionValueDialog extends ValueDialog {

    public final static String DIALOG_NAME = "life.diabet.diabetlife.dialog.value.InjectionValueDialog";

    @Override
    public String getDialogName() {
        return DIALOG_NAME;
    }

    @Override
    protected String getTitle() {
        return getResources().getString(R.string.injection_title);
    }

    @Override
    protected String getValueLabelText() {
        return getResources().getString(R.string.injection_unit);
    }

    @Override
    protected int getMinValue() {
        return getResources().getInteger(R.integer.injection_dialog_min_value);
    }

    @Override
    protected int getMaxValue() {
        return getResources().getInteger(R.integer.injection_dialog_max_value);
    }

    @Override
    protected String getDefaultValue() {
        return getResources().getString(R.string.injection_dialog_default_value);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        fractionSeparator.setVisibility(View.INVISIBLE);
        fractionValuePicker.setVisibility(View.INVISIBLE);

        return dialog;
    }
}
