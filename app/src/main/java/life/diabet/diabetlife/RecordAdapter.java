package life.diabet.diabetlife;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import life.diabet.diabetlife.util.FormatUtil;
import life.diabet.diabetlife.util.RecordComparatorByDate;

public class RecordAdapter extends ArrayAdapter<Record> {

    private Comparator<Record> comparator = new RecordComparatorByDate();

    public RecordAdapter(Context context, List<Record> objects) {
        super(context, -1, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Record record = getItem(position);

        if (convertView == null) {
            switch (record.getType()) {
                case Record.Types.SUGAR:
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.record_list_item_sugar, parent, false);
                    break;
                case Record.Types.FOOD:
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.record_list_item_food, parent, false);
                    break;
                default:
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.record_list_item_injection, parent, false);
                    break;
            }
        }
        ((TextView) convertView.findViewById(R.id.text1)).setText(FormatUtil.recordToString(getContext(), record));
        ((TextView) convertView.findViewById(R.id.text2)).setText(record.getTime());

        return convertView;
    }

    @Override
    public void addAll(@NonNull Collection<? extends Record> collection) {
        setNotifyOnChange(false);
        super.addAll(collection);
        sort(comparator);
        notifyDataSetChanged();
    }

    @Override
    public void insert(Record object, int index) {
        setNotifyOnChange(false);
        super.insert(object, index);
        sort(comparator);
        notifyDataSetChanged();
    }

    @Override
    public void add(Record object) {
        setNotifyOnChange(false);
        super.add(object);
        sort(comparator);
        notifyDataSetChanged();
    }

    @Override
    public int getViewTypeCount() {
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        Record record = getItem(position);
        switch (record.getType()) {
            case Record.Types.SUGAR:
                return 0;
            case Record.Types.FOOD:
                return 1;
            case Record.Types.INJECTION:
                return 2;
            default:
                return 0;
        }
    }
}
