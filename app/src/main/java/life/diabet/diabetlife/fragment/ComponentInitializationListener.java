package life.diabet.diabetlife.fragment;

public interface ComponentInitializationListener {

    void componentReady();
}
