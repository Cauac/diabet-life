package life.diabet.diabetlife.fragment;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;

import java.util.LinkedList;
import java.util.List;

import life.diabet.diabetlife.R;
import life.diabet.diabetlife.Record;
import life.diabet.diabetlife.RecordAdapter;

import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class RecordListFragment extends ListFragment implements RecordsFragment {

    private RecordAdapter adapter;
    private ComponentInitializationListener initializationListener;
    private OnItemClickListener onItemClickListener;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new RecordAdapter(getActivity(), new LinkedList<Record>());
        setListAdapter(adapter);

        setEmptyText(getResources().getString(R.string.no_data));
        ListView rawListView = (ListView) view.findViewById(android.R.id.list);
        rawListView.setPadding(0, 0, 0, 80);
        rawListView.setClipToPadding(false);

        initializationListener.componentReady();
    }

    @Override
    public void show(List<Record> records) {
        adapter.clear();
        adapter.addAll(records);
    }

    @Override
    public void add(Record record) {
        adapter.add(record);
    }

    @Override
    public void remove(Record record) {
        adapter.remove(record);
    }

    @Override
    public void insert(int position, Record record) {
        adapter.insert(record, position);
    }

    @Override
    public Record getRecord(int position) {
        return adapter.getItem(position);
    }

    @Override
    public void setItemClickListener(OnItemClickListener clickListener) {
        onItemClickListener = clickListener;
    }

    @Override
    public void setComponentInitializationListener(ComponentInitializationListener componentInitializationListener) {
        initializationListener = componentInitializationListener;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        onItemClickListener.onItemClick(l, v, position, id);
    }
}