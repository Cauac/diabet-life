package life.diabet.diabetlife.fragment;

import android.widget.AdapterView.OnItemClickListener;

import java.util.List;

import life.diabet.diabetlife.Record;

public interface RecordsFragment {

    void show(List<Record> records);

    void add(Record record);

    void remove(Record record);

    void insert(int position, Record record);

    Record getRecord(int position);

    void setItemClickListener(OnItemClickListener clickListener);

    void setComponentInitializationListener(ComponentInitializationListener componentInitializationListener);
}
