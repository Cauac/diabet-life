package life.diabet.diabetlife.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.List;

import life.diabet.diabetlife.R;
import life.diabet.diabetlife.Record;

public class RecordChartFragment extends Fragment implements RecordsFragment {

    private LineChart chart;
    private ComponentInitializationListener componentInitializationListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.chart_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        chart = (LineChart) view.findViewById(R.id.chart);
        componentInitializationListener.componentReady();
    }

    @Override
    public void show(List<Record> records) {
        List<String> values = new ArrayList<>(records.size());
        List<String> xVals = new ArrayList<String>(records.size());
        ArrayList<Entry> yVals = new ArrayList<Entry>();
        int index = 0;
        for (Record record : records) {
            xVals.add(record.getTime());
            yVals.add(new Entry(Float.parseFloat(record.getValue()), index++));
        }

        LineDataSet dataSet = new LineDataSet(yVals, "Sugar");
        LineData data = new LineData(xVals, dataSet);
        chart.setData(data);
    }

    @Override
    public void add(Record record) {

    }

    @Override
    public void remove(Record record) {

    }

    @Override
    public void insert(int position, Record record) {

    }

    @Override
    public Record getRecord(int position) {
        return null;
    }

    @Override
    public void setItemClickListener(AdapterView.OnItemClickListener clickListener) {

    }

    @Override
    public void setComponentInitializationListener(ComponentInitializationListener componentInitializationListener) {
        this.componentInitializationListener = componentInitializationListener;
    }
}
