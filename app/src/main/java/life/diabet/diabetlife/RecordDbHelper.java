package life.diabet.diabetlife;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.LinkedList;
import java.util.List;

import life.diabet.diabetlife.Record.RecordEntry;

public class RecordDbHelper extends SQLiteOpenHelper {

    protected static int DATABASE_VERSION;
    protected static String DATABASE_NAME;
    protected static String SQL_CREATE_RECORD_TABLE;
    protected static String SQL_DELETE_RECORD_TABLE;
    protected static String SQL_RECORD_LIST;

    private SQLiteDatabase database;

    public static RecordDbHelper getInstance(Context context) {
        if (DATABASE_NAME == null) {
            DATABASE_NAME = context.getResources().getString(R.string.database_name);
            DATABASE_VERSION = context.getResources().getInteger(R.integer.database_version);

            SQL_CREATE_RECORD_TABLE = context.getResources().getString(R.string.sql_create_record_table_query);
            SQL_DELETE_RECORD_TABLE = context.getResources().getString(R.string.sql_delte_record_table_query);
            SQL_RECORD_LIST = context.getResources().getString(R.string.sql_record_list_query);
        }
        return new RecordDbHelper(context);
    }

    private RecordDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        database = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_RECORD_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_RECORD_TABLE);
        onCreate(db);
    }

    public List<Record> list(String date) {
        List<Record> records = new LinkedList<Record>();
        Cursor cursor = database.rawQuery(SQL_RECORD_LIST, new String[]{date});
        if (cursor.moveToFirst()) {
            do {
                Record record = new Record();
                record.setIdentity(cursor.getLong(0));
                record.setDate(cursor.getString(1));
                record.setTime(cursor.getString(2));
                record.setValue(cursor.getString(3));
                record.setType(cursor.getString(4));
                record.setStress(cursor.getString(5));
                record.setActivity(cursor.getString(6));
                record.setComment(cursor.getString(7));
                System.out.println(record);
                records.add(record);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return records;
    }

    protected ContentValues getRecordValues(Record record) {
        ContentValues values = new ContentValues();
        values.put(RecordEntry.COLUMN_NAME_DATE, record.getDate());
        values.put(RecordEntry.COLUMN_NAME_TIME, record.getTime());
        values.put(RecordEntry.COLUMN_NAME_VALUE, record.getValue());
        values.put(RecordEntry.COLUMN_NAME_TYPE, record.getType());
        values.put(RecordEntry.COLUMN_NAME_STRESS, record.getStress());
        values.put(RecordEntry.COLUMN_NAME_ACTIVITY, record.getActivity());
        values.put(RecordEntry.COLUMN_NAME_COMMENT, record.getComment());
        return values;
    }

    public void save(Record record) {
        long newRowId = database.insert(RecordEntry.TABLE_NAME, null, getRecordValues(record));
        record.setIdentity(newRowId);
    }

    public void update(Record record) {
        String selection = RecordEntry._ID + " = ?";
        String[] selectionArgs = {String.valueOf(record.getIdentity())};
        database.update(RecordEntry.TABLE_NAME, getRecordValues(record), selection, selectionArgs);
    }

    public void delete(Record record) {
        String selection = RecordEntry._ID + " = ?";
        String[] selectionArgs = {String.valueOf(record.getIdentity())};
        database.delete(RecordEntry.TABLE_NAME, selection, selectionArgs);
    }
}
