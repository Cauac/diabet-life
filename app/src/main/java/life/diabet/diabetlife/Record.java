package life.diabet.diabetlife;

import android.provider.BaseColumns;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

public class Record implements Serializable {

    public final class Types {
        public static final String SUGAR = "sugar";
        public static final String FOOD = "food";
        public static final String INJECTION = "injection";
    }

    public static abstract class RecordEntry implements BaseColumns {
        public static final String TABLE_NAME = "records";
        public static final String COLUMN_NAME_DATE = "date";
        public static final String COLUMN_NAME_TIME = "time";
        public static final String COLUMN_NAME_VALUE = "value";
        public static final String COLUMN_NAME_TYPE = "type";
        public static final String COLUMN_NAME_STRESS = "stress";
        public static final String COLUMN_NAME_ACTIVITY = "activity";
        public static final String COLUMN_NAME_COMMENT = "comment";
    }

    private long identity;
    private String date;
    private String time;
    private String type;
    private String value;
    private String stress;
    private String activity;
    private String comment;

    public void setIdentity(long identity) {
        this.identity = identity;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setStress(String stress) {
        this.stress = stress;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getIdentity() {
        return identity;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public String getStress() {
        return stress;
    }

    public String getActivity() {
        return activity;
    }

    public String getComment() {
        return comment;
    }

    public String toString() {
        return StringUtils.join(new String[]{"" + identity, date, time, type, value, stress, activity, comment}, ",");
    }

    public void merge(Record record) {
        setIdentity(record.getIdentity());
        setDate(record.getDate());
    }
}
