package life.diabet.diabetlife.util;

import java.util.Comparator;

import life.diabet.diabetlife.Record;

public class RecordComparatorByDate implements Comparator<Record> {

    @Override
    public int compare(Record firstRecord, Record secondRecord) {
        return firstRecord.getTime().compareTo(secondRecord.getTime());
    }
}
