package life.diabet.diabetlife.util;

import android.content.Context;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;

import java.util.Calendar;
import java.util.Date;

import life.diabet.diabetlife.R;
import life.diabet.diabetlife.Record;

public class FormatUtil {

    public static final FastDateFormat dateFormat = FastDateFormat.getInstance("dd/MM/yyyy");
    public static final FastDateFormat timeFormat = FastDateFormat.getInstance("HH:mm");

    public static String dateToString(Date date) {
        return dateFormat.format(date);
    }

    public static String currentTime() {
        return timeFormat.format(new Date());
    }

    public static String intToStringTime(int hourOfDay, int minutes) {
        return String.format("%02d:%02d", hourOfDay, minutes);
    }

    public static Calendar stringToTime(String timeStr) {
        Calendar calendar = Calendar.getInstance();
        if (StringUtils.isNoneBlank(timeStr)) {
            String[] split = timeStr.split(":");
            calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(split[0]));
            calendar.set(Calendar.MINUTE, Integer.valueOf(split[1]));
        }
        return calendar;
    }

    public static String recordToString(Context context, Record record) {
        String recordStr = "%s: %s %s";
        switch (record.getType()) {
            case Record.Types.SUGAR:
                recordStr = String.format(recordStr, context.getResources().getString(R.string.Sugar), record.getValue(), context.getResources().getString(R.string.sugar_unit));
                break;
            case Record.Types.FOOD:
                recordStr = String.format(recordStr, context.getResources().getString(R.string.Food), record.getValue(), context.getResources().getString(R.string.food_unit));
                break;
            case Record.Types.INJECTION:
                recordStr = String.format(recordStr, context.getResources().getString(R.string.Injection), record.getValue(), context.getResources().getString(R.string.injection_unit));
                break;
        }
        return recordStr;
    }

    public static String recordValueToString(String value, String unit) {
        return String.format("%s %s", value, unit);
    }

    public static String extractValue(String valueStr) {
        return valueStr.split(" ")[0];
    }
}
