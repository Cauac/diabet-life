package life.diabet.diabetlife.util;

import android.util.SparseArray;

import java.util.HashMap;
import java.util.Map;

import life.diabet.diabetlife.R;
import life.diabet.diabetlife.Record;
import life.diabet.diabetlife.dialog.record.EditFoodActivity;
import life.diabet.diabetlife.dialog.record.EditInjectionActivity;
import life.diabet.diabetlife.dialog.record.EditSugarActivity;

public class UiUtil {

    public static final Map<String, Class> RECORD_DIALOGS = new HashMap<String, Class>() {{
        put(Record.Types.SUGAR, EditSugarActivity.class);
        put(Record.Types.FOOD, EditFoodActivity.class);
        put(Record.Types.INJECTION, EditInjectionActivity.class);
    }};

    public static final SparseArray<Class> SHEET_ITEMS = new SparseArray<Class>() {{
        put(R.id.addSugar, EditSugarActivity.class);
        put(R.id.addFood, EditFoodActivity.class);
        put(R.id.addInjection, EditInjectionActivity.class);
    }};
}
